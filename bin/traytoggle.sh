#!/bin/bash
# toggle visibility of stalonetray
if ! xdotool search --onlyvisible --name "stalonetray" windowunmap ; then
   eval $(xdotool getmouselocation --shell 2> /dev/null)
   xdotool search --name "stalonetray" windowmap windowmove $X $Y windowactivate
fi
